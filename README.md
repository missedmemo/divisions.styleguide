# ✨ DMG Style Guide <span style="font-size:0.6em;">(built with Styleguidist)</span>

<span style="color:#BBB;">React TypeScript components, styled with Tailwind CSS, tested with Jest and the React Testing Library</span>

## Create/Edit a component

1. create/update your component (ex: `mybutton.tsx` & `mybutton.md`) in `src/components`
2. type "yarn view" in the terminal and navigate to `localhost:6060` to interact with components
3. document your component [(see guidelines)](https://react-styleguidist.js.org/docs/documenting)

## Test your component

1. create/update `mybutton.test.tsx` in `tests` folder (\*.test.\* files are ignored by Styleguidist)
2. type "yarn test" to run all component tests

## Publish updated Style Guide as a stand-alone static website

1. "yarn build:styleguide" will generate a static styleguide HTML site (in `public` folder)<br>
   (performed automatically, on every push of code changes, hosted on a GitLab Pages url)
2. TO DO: create a more realistic set of components and themes/styles to evaluate interop with clients

## Publish a library of themes, styles, and components to a shared project folder

1. update `src/index.ts` with a reference to your component: `export { default as MyButton } from "./components/mybutton";`
2. "yarn build:componentlib" will generate a component library (in `component.lib` folder)<br>
   (performed automatically, on every push of code changes)
3. TO DO: possibly define some process to VALIDATE updated components across all client projects?

---

**_NOTE:_**

```
Styleguidist FAILS TO RUN using the latest (5.x) webpack version,
so we use the final 4.x version in building this project instead.

Other libs are up-to-date, as of `10/15/2020`

Project is not based on create-react-app, which can result in greater control
over a minimal set of more up-to-date dependencies -- but a future version of
CRA could be switched in again later, if it's determined to be more convenient.
```
