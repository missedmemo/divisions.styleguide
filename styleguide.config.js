const { resolve } = require("path");

const TAILWIND_STYLES = resolve("src/tailwind.css");

module.exports = {
  require: [TAILWIND_STYLES],
  styleguideDir: "public",
  webpackConfig: {
    devtool: "inline-source-map",
    module: {
      rules: [
        {
          test: /\.tsx?$/,
          loader: "ts-loader",
          exclude: "/node_modules/",
        },
        {
          test: /\.css$/,
          use: ["style-loader", "css-loader", "postcss-loader"],
        },
      ],
    },
    resolve: {
      extensions: [".tsx", ".ts", ".js"],
    },
  },
};
