import React from "react";
import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";

import ToggleButton, { IToggleButton } from "../src/components/togglebutton";

test("should toggle between expected DEFAULT values", () => {
  passProps({});
});

test("should toggle between CUSTOM values provided", () => {
  passProps({ txtToggled: "Off", txtNotToggled: "On" });
});

test("should support ACTIVE initial toggle state", () => {
  passProps({ toggled: true });
});

const passProps = (props: IToggleButton) => {
  const { initialText, secondaryText } = expectedText(props);

  render(<ToggleButton {...props} />);
  const btnToggle = screen.getByRole("button");

  userEvent.click(btnToggle);
  expect(btnToggle).toHaveTextContent(secondaryText);

  userEvent.click(btnToggle);
  expect(btnToggle).toHaveTextContent(initialText);

  userEvent.click(btnToggle);
  expect(btnToggle).toHaveTextContent(secondaryText);
};

const expectedText = ({
  toggled = false,
  txtNotToggled = "Start",
  txtToggled = "Stop",
}: IToggleButton) => ({
  initialText: toggled ? txtToggled : txtNotToggled,
  secondaryText: toggled ? txtNotToggled : txtToggled,
});
